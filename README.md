# Teste Backend 01

## Escopo

Deve-se configurar uma taxonomia personalizada para posts dentro do WordPress, de forma a categorizar os posts por regiões. Além disso, é imprescindível que os arquives e singles de posts contenham a informação de qual região os posts pertencem na URL.
Para finalizar, deve-se disponibilizar tais informações via REST API para facilitarmos a integração de nosso sistema com outros serviços de terceiros.


## Cenário fictício

A empresa TaoPress entrou em contato com a Seox para construção de um site de publicação de notícias. Hoje eles têm diversos sites, uma para cada estado do país, e querem unificá-los em um. Eles desejam isso pois também querem integrar o aplicativo móvel da empresa com as notícias do portal e, com a base de dados centralizada, fica muito mais fácil consumir esses dados. 

Os sites de notícias deles já têm diversas categorias (ex: esportes, política, etc.) e, por isso, para segregar notícias de cada estado, eles acham melhor categorizar os posts a partir de uma outra taxonomia que não a default de posts (Categories e Tags).


## Requisitos

Abaixo estão listados os requisitos funcionais e não funcionais do sistema. Caso você tenha alguma sugestão de funcionalidade que substitua um dos requisitos ou os tornem desnecessários, basta entrar em contato com o RH para verificar se é possível fazer essa troca. O mesmo é válido para qualquer dúvida que você tenha sobre os requisitos.


### Requisitos Funcionais

RF1: Criar uma nova taxonomia de post chamada Regiões. Essa taxonomia precisa ser hierárquica (ter sub termos).
```
Exemplo da taxonomia hierárquica de regiões:
- Rio Grande do Sul
    - Porto Alegre
    - Canoas
    - Gravataí
- São Paulo
    - Capital
    - Santos
- Santa Catarina
    - Lages
    - Florianópolis
    - São José
```

RF2: Deve-se fazer um rewrite da estrutura da url para conter essa taxonomia no início do path de páginas como arquive e single de post
```
Ex: Arquive e Single de notícias de São Paulo Capital
Arquive: /sao-paulo/capital/noticias/
Single: /sao-paulo/capital/noticias/noticia-1/
Single: /sao-paulo/capital/noticias/noticia-2/

O path `/noticias/` refere-se a uma palavra estática no PATH, ou uma categoria padrão de post (taxonomia category) que, idealmente, deve também estar presente no PATH do post.
```

RF3: Caso o post não tenha termos dessa taxonomia setados, não deve aparecer nada dessa taxonomia no path.
```
Ex: Se o post for, por exemplo, de todos os estados (notícia nacional), nenhum termo da taxonomia regiões seria selecionado. Sendo assim, essa notícia não deveria ter o nome do termo no PATH.
Single: /noticias/noticia-1/
Single: /noticias/noticia-2/
```

RF4: Retornar todos os posts filtrados com base em termos da taxonomia região através de uma REST API.
```
Como através da rota `/wp-json/wp/v2/posts?categories=20` podemos filtrar posts por categorias, vamos precisar de uma rota que filtre posts por regiões
```

RF5: Na rota de detalhes de um post, os termos de Região (taxonomia criada) devem ser retornados (da mesma forma que as categorias são, por exemplo)


### Requisitos Não Funcionais

RNF1: Toda a configuração de taxonomias, rewrite de url e REST (se preciso) devem ser armazenadas em um plugin, facilitando a portabilidade do sistema.

RNF2: A documentação das rotas da REST API devem ser entregues através de um projeto no Insomnia ou Postman ou qualquer outro do gênero.


## Limitações/Requisitos de qualidade

1. Você deve utilizar o padrão de codificação WordPress no desenvolvimento da aplicação WordPress. Ref: https://github.com/WordPress/WordPress-Coding-Standards

    Dentro deste repo configuramos um arquivo composer.json com os scripts de verificação de qualidade de código. Para verificar se o projeto está seguindo o padrão definido, rode o seguinte comando no root do projeto: `composer check`.

2. Busque utilizar o padrão PSR-4 para implementação de classes no sistema. Já configuramos o autoload com PSR-4 dentro do arquivo composer.json 

3. Configure um arquivo README.md no plugin do projeto, explicando:
    - Author e versão do plugin.
    - Como instalar e executar aplicação.
    - Como você organizou o projeto. (Como organizou as pastas? Utilizou algum padrão de projeto, como, por exemplo, MVC?)
    - Modelagem do DB pronta.
    - Observações do autor (opcional).


## Por onde começar

1. Faça um fork deste repositório e utilize-o para realizar a implementação da solução (plugin WP).
2. Utilize o README para adicionar detalhamentos de como executar o seu projeto, além de comentários livres que você queira adicionar.


## Entrega

1. Deve-se apenas entregar o plugin utilizado para customizar o WordPress e, se alguma configuração foi feita pelo painel do WordPress, ou por algum outro plugin, basta documentá-la no README do plugin.
2. Adicione no README do plugin um link de um vídeo explicativo de como o sistema funciona
3. Enviar, via email rh@seox.com.br, o link do repositório originado deste (fork) com os códigos da solução implementada.
4. Hospede a sua aplicação em um ambiente online para que possamos realizar testes funcionais da solução e envie a link da aplicação para o email rh@seox.com.br. Se houver alguma dúvida de como hospedar, entre em contato através do email rh@seox.com.br para tirar as suas dúvidas.


## Entregas extras

- Cubra pelo menos 3 funções principais de qualquer um dos dois projetos com testes unitários.
- Considere os princípios SOLID enquanto estiver desenvolvendo.
- Faça um diagrama de classes/pacotes do seu projeto.
- Utilize padrões de projetos conhecidos, como por exemplo "Factories e Singletons".


## Observações

- Busque cumprir o prazo que você estimou para entrega do teste.
- Traga sugestões, se você achar o teste muito complexo ou pouco claro.
- Não esqueça que ao final de tudo o objetivo é entregar o produto ao cliente, mesmo que você tenha que trilhar um caminho técnico um pouco diferente do que foi definido aqui.
- Entre em contato pelo rh@seox.com.br se tiver qualquer dúvida sobre o teste.
